import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Task } from "./task.model";
import { AuthService } from '../../../providers/auth-service'

/*
  Generated class for the Tasks provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TaskService {
  private resourceUrl: string = 'https://api.github.com'
  // protected requestInstance
  
  constructor(
    public http: Http,
    public authService: AuthService
    ) {
    if (!this.authService.isAuthd) {
      // throw new Error('Expects a valid authentic session')
    }
  }

  makeRequest (uri: string, method: string = 'GET', extraHeaders: any = {}, body?: Object): Observable<any> {
    extraHeaders.Authorization = 'token' + this.authService.authData().token
    return this.http.request(`${this.resourceUrl}${uri}`, {
      url: `${this.resourceUrl}.${uri}`,
      method: method,
      headers: new Headers(extraHeaders),
      body: body
    })
  }

  /**
   * fetches all tasks / issues from github
   */
  getMyTasks (): Observable<Task[]> {
    return this.makeRequest('/issues')
      // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      //...errors if any
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }  

  selectRepos (): Observable<any[]> {
    return this.http.get(this.resourceUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));    
  }

}
