export class Task {
  constructor(
    public id: String,
    public author: String,
    public title: String,
    public source: String,
    public createdAt: Date,
    public lastModified: Date,
    public properties: any
  ) {

  }
}