import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { IonicModule} from 'ionic-angular';


import { TaskComponent } from './task.component';
import { TaskService } from './shared/task.service'

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    IonicModule
  ],
  exports: [TaskComponent],
  declarations: [TaskComponent],
  providers: [TaskService],
})
export class TaskModule { }
