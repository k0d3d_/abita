import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TaskService } from './tasks/shared/task.service'
import { TaskModule } from './tasks/tasks.module'
import { AuthPage } from '../pages/auth/auth'

import {AuthService} from '../providers/auth-service.ts'
import { EmitterService } from '../emitter.service'

// Must export the config
export const firebaseConfig = {
    apiKey: "AIzaSyCo2U4WFS0h0sKM1fWHc0IohuJA0wckQ7o",
    authDomain: "abita-2e0c2.firebaseapp.com",
    databaseURL: "https://abita-2e0c2.firebaseio.com",
    storageBucket: "abita-2e0c2.appspot.com",
    messagingSenderId: "841250545290"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AuthPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    TaskModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AuthPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AuthService, EmitterService, TaskService]
})
export class AppModule {}
