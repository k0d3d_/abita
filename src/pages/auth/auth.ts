import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AuthService } from '../../providers/auth-service' 
import { HomePage } from '../home/home';

/*
  Generated class for the Auth page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
  providers: [AuthService]
})
export class AuthPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public authService: AuthService) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  signInWithGithub (): void {
    this.authService.signInWithGithub()
    .then((r) => {
      // console.log(r)
      window.localStorage.setItem('authToken', r.github.accessToken)
      this.navCtrl.push(HomePage);
    }, authdError => {
      this.handleError(authdError)
    })
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }  

}
