import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { AuthPage } from '../auth/auth'
import { TaskService } from '../../app/tasks/shared/task.service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  items: any;
  
  constructor(public navCtrl: NavController, public tasks: TaskService) {
    // this.items = this.tasks.getMyTasks()
    // this.items.catch(e => {
    //   console.log(e);
    // })

  }
  
  goToLogin () {
    this.navCtrl.push(AuthPage);
  }

  goToHome () {
    this.navCtrl.push(HomePage);
  }

  reloadTasks () {
    this.items = this.tasks.getMyTasks()
  }

}
