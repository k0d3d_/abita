import { Injectable } from '@angular/core';
import { AuthProviders, AngularFireAuth, FirebaseAuthState, AuthMethods } from 'angularfire2';

@Injectable()
export class AuthService {
  private authState: FirebaseAuthState;

  constructor(public auth$: AngularFireAuth) {
    this.authState = auth$.getAuth();
    auth$.subscribe((state: FirebaseAuthState) => {
      this.authState = state;
    });
  }

  /**
   * quick check if an authenticated session is present 
   */
  get isAuthd(): boolean {
    return this.authState !== null;
  }
  /**
   * returns the authenticated user basic information
   */
  authData(): any {
    if (!this.authState) return {}
    return {
      id: this.authState.uid,
      token: window.localStorage.getItem('authToken')
    }
  }

  signInWithGithub(): firebase.Promise<FirebaseAuthState> {
    return this.auth$.login({
      provider: AuthProviders.Github,
      method: AuthMethods.Popup     
    })
  }

  signOut(): void {
    this.auth$.logout();
  }
}